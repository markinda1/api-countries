import axios from 'axios';

const url = 'https://restcountries.com/v3.1/';

export const apiGetCountries = async () => await axios.get<ICountry[]>(url + 'all');

export const apiGetCountry = async (name: string) =>
    await axios.get<ICountry[]>(url + 'name/' + name);
