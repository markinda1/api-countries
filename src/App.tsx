import React from 'react';
import { Provider } from 'react-redux';
import '~/scss/index.scss';
import Routing from '~/Routing';
import store from '~/store/store';
import Layout from '~/components/Layout/Layout';

const App = () => (
    <Provider store={store} key="provider">
        <Layout>
            <Routing />
        </Layout>
    </Provider>
);

export default App;
