import { createSlice } from '@reduxjs/toolkit';

export interface IInitialState {
    theme: 'light' | 'dark';
}

const initialState: IInitialState = {
    theme: 'light',
};

const appSlice = createSlice({
    name: 'app',
    initialState,
    reducers: {
        lightTheme: (state: IInitialState) => {
            state.theme = 'light';
        },
        darkTheme: (state: IInitialState) => {
            state.theme = 'dark';
        },
    },
});

export const { lightTheme, darkTheme } = appSlice.actions;

export default appSlice;
