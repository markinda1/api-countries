import { createSlice } from '@reduxjs/toolkit';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { apiGetCountries } from '~/api/countries';
import { PayloadAction } from '@reduxjs/toolkit';
import { apiGetCountry } from '~/api/countries';

export interface IInitialState {
    list: ICountry[];
    loadList: boolean;
    filter: ICountry[];
    current: {
        load: boolean;
        item?: ICountry;
    };
}

const initialState: IInitialState = {
    list: [],
    loadList: false,
    filter: [],
    current: {
        load: false,
        item: undefined,
    },
};

export const getCountries = createAsyncThunk<ICountry[]>('country/get', async () => {
    const response = await apiGetCountries();
    return response.data;
});

export const getCountry = createAsyncThunk<ICountry[], string>('country/getName', async (name) => {
    const response = await apiGetCountry(name);
    return response.data;
});

const countrySlice = createSlice({
    name: 'country',
    initialState,
    reducers: {
        filterCountry: (
            state: IInitialState,
            action: PayloadAction<{ region: string; value: string }>,
        ) => {
            const payload = action.payload;

            let data = [...state.list];

            if (payload.region) {
                data = data.filter((c) => c.region.includes(payload.region));
            }

            if (payload.value) {
                data = data.filter((country) =>
                    country.name.common.toLowerCase().includes(payload.value.toLowerCase()),
                );
            }

            state.filter = data;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getCountries.pending, (state: IInitialState) => {
                state.loadList = true;
            })
            .addCase(getCountries.rejected, (state: IInitialState) => {
                state.loadList = false;
                console.error('getCountries REJECTED');
            })
            .addCase(getCountries.fulfilled, (state: IInitialState, action) => {
                const payload = action.payload;

                if (!payload) return state;

                state.list = payload;
                state.loadList = false;
                state.filter = payload;
            })
            .addCase(getCountry.pending, (state: IInitialState) => {
                state.current.load = true;
            })
            .addCase(getCountry.rejected, (state: IInitialState) => {
                state.current.load = false;
                console.error('getCountry REJECTED');
            })
            .addCase(getCountry.fulfilled, (state: IInitialState, action) => {
                const payload = action.payload;

                if (!payload) return state;

                state.current.load = false;
                state.current.item = payload[0];
            });
    },
});

export const { filterCountry } = countrySlice.actions;

export default countrySlice;
