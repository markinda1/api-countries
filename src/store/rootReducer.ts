import { configureStore, combineReducers } from '@reduxjs/toolkit';
import appSlice from '~/store/Slice/AppSlice';
import countrySlice from '~/store/Slice/CountrySlice';

const rootReducer = combineReducers({
    app: appSlice.reducer,
    country: countrySlice.reducer,
});

export const setupStore = () => {
    return configureStore({
        reducer: rootReducer,
        middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
    });
};

export default rootReducer;
