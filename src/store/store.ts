import { persistReducer } from 'redux-persist';
import createCompressor from 'redux-persist-transform-compress';
import storage from 'redux-persist/lib/storage';
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '~/store/rootReducer';

const compressor = createCompressor();

const persistConfig = {
    key: 'api-countries',
    storage,
    transforms: process.env.NODE_ENV === 'development' ? [] : [compressor],
    whitelist: [],
};

const confStore = () => {
    const reducer = persistReducer(persistConfig, (state, action) => rootReducer(state, action));

    return configureStore({
        reducer,
    });
};

const store = confStore();

export default {
    ...store,
};
