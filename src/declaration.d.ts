declare module '*.scss' {
    const styles: { [key: string]: string };
    export default styles;
}

declare module '*.css' {
    const styles: { [key: string]: string };
    export default styles;
}

declare module '*.svg' {
    import React = require('react');
    export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>;
    const src: string;
    export default src;
}
