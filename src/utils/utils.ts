export const prepareOptions = (options: ISelectOption) => {
    const array = Object.keys(options).map((key) => {
        return { value: key, label: options[key] };
    });

    const empty = array.findIndex((elem) => elem.value === '');

    if (empty !== -1) {
        array.splice(0, 0, array.splice(empty, 1)[0]);
    }

    return array;
};
