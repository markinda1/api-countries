import React from 'react';
import { useDocumentTitle, useDebounce } from 'usehooks-ts';
import styles from './Main.module.scss';
import Input from '~/components/form/Input/Input';
import { INPUT_TYPE } from '~/const';
import SelectField from '~/components/form/Select/SelectField';
import useAppSelector from '~/hooks/useAppSelector';
import useAppDispatch from '~/hooks/useAppDispatch';
import { getCountries, filterCountry } from '~/store/Slice/CountrySlice';
import Card from '~/components/country/Card';
import Loader from '~/components/loader/Loader';

const Main = () => {
    useDocumentTitle('react build');
    const dispatch = useAppDispatch();
    const countries = useAppSelector((state) => state.country.filter);
    const load = useAppSelector((state) => state.country.loadList);
    const [value, setValue] = React.useState<string>('');
    const debouncedValue = useDebounce<string>(value, 400);
    const [region, setRegion] = React.useState<string>('');
    const preparedGroups: ISelectOption = {};

    const options = [
        { value: 'Africa', label: 'Africa' },
        { value: 'Americas', label: 'Americas' },
        { value: 'Asia', label: 'Asia' },
        { value: 'Europe', label: 'Europe' },
        { value: 'Oceania', label: 'Oceania' },
    ];

    options.forEach((item) => {
        preparedGroups[item.value] = item.label;
    });

    React.useEffect(() => {
        if (countries.length === 0) {
            dispatch(getCountries());
        }
    }, [dispatch]);

    React.useEffect(() => {
        dispatch(filterCountry({ value: debouncedValue, region }));
    }, [dispatch, region, debouncedValue]);

    return (
        <div className={styles.main}>
            <div className={styles.mainControls}>
                <Input
                    iconType={INPUT_TYPE.SEARCH}
                    useIcon
                    placeholder="Search for a country..."
                    value={value}
                    onChange={setValue}
                    className={styles.mainControlsInput}
                />
                <SelectField
                    options={preparedGroups}
                    placeholder="Filter by Region"
                    defaultValue={region}
                    onChange={(value) => setRegion(value?.value ?? '')}
                    className={styles.mainControlsSelect}
                />
            </div>
            {load ? (
                <Loader className={styles.mainGridLoader} />
            ) : (
                <div className={styles.mainGrid}>
                    {!!countries.length ? (
                        countries?.map((country, index) => (
                            <Card
                                country={{ ...country }}
                                className={styles.mainGridCard}
                                key={index}
                            />
                        ))
                    ) : (
                        <p className={styles.mainGridNone}>Ничего не найдено...</p>
                    )}
                </div>
            )}
        </div>
    );
};

export default Main;
