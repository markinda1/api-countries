import React from 'react';
import { useDocumentTitle } from 'usehooks-ts';
import styles from './Country.module.scss';
import { useParams, useNavigate } from 'react-router-dom';
import useAppSelector from '~/hooks/useAppSelector';
import { IoArrowBack } from 'react-icons/io5';
import { getCountry } from '~/store/Slice/CountrySlice';
import useAppDispatch from '~/hooks/useAppDispatch';
import Loader from '~/components/loader/Loader';
import BorderCountries from '~/components/country/BorderCountries';

const Country = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const country = useAppSelector((state) => state.country.current.item);
    const load = useAppSelector((state) => state.country.current.load);
    const name = useParams().name || '';

    useDocumentTitle(name);

    React.useEffect(() => {
        dispatch(getCountry(name));
    }, [dispatch, name]);

    const nativeName = Object.values(country?.name?.nativeName || {});
    const currency = Object.values(country?.currencies || {});
    const languages = Object.values(country?.languages || {});

    return (
        <div className={styles.country}>
            {load ? (
                <Loader className={styles.countryLoader} />
            ) : (
                <>
                    <div className={styles.countryControls}>
                        <div className={styles.countryBack} onClick={() => navigate(-1)}>
                            <IoArrowBack /> Back
                        </div>
                    </div>
                    <div className={styles.countryContent}>
                        <div className={styles.countryContentImage}>
                            <img src={country?.flags.png} alt={country?.flags.alt} />
                        </div>
                        <div className={styles.countryContentInfo}>
                            <h1>{country?.name.common}</h1>
                            <div className={styles.countryContentLists}>
                                <ul>
                                    <li>
                                        <strong>Native Name: </strong>
                                        {nativeName[nativeName.length - 1]?.common}
                                    </li>
                                    <li>
                                        <strong>Population: </strong>
                                        {country?.population}
                                    </li>
                                    <li>
                                        <strong>Region: </strong>
                                        {country?.region}
                                    </li>
                                    <li>
                                        <strong>Sub Region: </strong>
                                        {country?.subregion}
                                    </li>
                                    <li>
                                        <strong>Capital: </strong>
                                        {country?.capital}
                                    </li>
                                </ul>
                                <ul>
                                    <li>
                                        <strong>Top Level Domain: </strong>
                                        {country?.tld}
                                    </li>
                                    <li>
                                        <strong>Currency: </strong>
                                        {currency?.map((c: any) => c.name)}
                                    </li>
                                    <li>
                                        <strong>Languages: </strong>
                                        {languages?.join(', ')}
                                    </li>
                                </ul>
                            </div>
                            <BorderCountries
                                borders={country?.borders}
                                className={styles.countryBorder}
                            />
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};

export default Country;
