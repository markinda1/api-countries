import React from 'react';
import styles from './Loader.module.scss';
import classNames from 'classnames';

interface ILoader {
    className?: string;
}

const Loader = ({ className }: ILoader) => <div className={classNames(styles.loader, className)} />;

export default Loader;
