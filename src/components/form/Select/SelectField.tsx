import * as React from 'react';
import Select from 'react-select';
import { prepareOptions } from '~/utils/utils';

interface ISelect {
    options: ISelectOption;
    defaultValue?: string;
    placeholder?: string;
    onChange: (value: ISelectValue) => void;
    className?: string;
}

const SelectsField = ({
    options,
    defaultValue,
    placeholder = '',
    onChange,
    className,
}: ISelect) => {
    if (!options) {
        return null;
    }

    const innerPreparedOptions = prepareOptions(options);
    const _defaultValue = innerPreparedOptions.find((option) => option.value == defaultValue);

    return (
        <Select
            className={className}
            options={innerPreparedOptions}
            value={_defaultValue}
            onChange={(newValue) => onChange(newValue)}
            placeholder={placeholder}
            isClearable={true}
            styles={{
                control: (provided) => ({
                    ...provided,
                    backgroundColor: 'var(--colors-ui-base)',
                    color: 'var(--colors-text)',
                    borderRadius: '8px',
                    border: 'none',
                    boxShadow: 'var(--shadow)',
                    height: '100%',
                }),
                valueContainer: (provided) => ({
                    ...provided,
                    fontSize: '14px',
                    paddingLeft: '22px',
                }),
                singleValue: (provided) => ({
                    ...provided,
                    color: 'var(--colors-text)',
                    margin: '0',
                }),
                menu: (provided) => ({
                    ...provided,
                    boxShadow: 'var(--shadow)',
                    border: 'none',
                    borderRadius: '8px',
                    borderTop: 'none',
                    backgroundColor: 'var(--colors-ui-base)',
                    zIndex: '99',
                }),
                option: (provided, state) => ({
                    ...provided,
                    cursor: 'pointer',
                    color: 'var(--colors-text)',
                    padding: '6px 22px',
                    fontSize: '14px',
                    backgroundColor: state.isSelected
                        ? 'var(--colors-bg)'
                        : 'var(--colors-ui-base)',
                    ':active': {
                        backgroundColor: 'transparent',
                    },
                }),
            }}
        />
    );
};

export default SelectsField;
