import React from 'react';
import styles from './Input.module.scss';
import classNames from 'classnames';
import Icon from '~/components/icon/Icon';

interface IInput {
    value?: string;
    onChange?: (value: string) => void;
    placeholder?: string;
    useIcon?: boolean;
    iconType?: string;
    type?: string;
    className?: string;
}

const Input = ({ iconType, placeholder, useIcon, type, value, onChange, className }: IInput) => {
    return (
        <div className={classNames(styles.input, className)}>
            {useIcon && <Icon className={styles.inputIcon} type={iconType} />}
            <input
                className={styles.inputInput}
                type={type || 'text'}
                placeholder={placeholder}
                value={value}
                onChange={(event) => onChange && onChange(event.target.value)}
            />
        </div>
    );
};

export default Input;
