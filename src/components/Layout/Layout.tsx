import React from 'react';
import styles from './Layout.module.scss';
import Theme from '~/components/Layout/Theme';

interface ILayout {
    children: React.ReactNode;
}

const Layout = ({ children }: ILayout) => {
    return (
        <div className={styles.layout}>
            <header className={styles.layoutHeader}>
                <div className={styles.layoutHeaderContainer}>
                    <h1 className={styles.layoutHeaderTitle}>Where in the world?</h1>
                    <Theme />
                </div>
            </header>
            <main className={styles.layoutMain}>{children}</main>
        </div>
    );
};

export default Layout;
