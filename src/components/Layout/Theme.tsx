import styles from '~/components/Layout/Layout.module.scss';
import { IoMoonOutline } from 'react-icons/io5';
import { IoMoon } from 'react-icons/io5';
import React from 'react';
import useAppDispatch from '~/hooks/useAppDispatch';
import useAppSelector from '~/hooks/useAppSelector';
import { darkTheme } from '~/store/Slice/AppSlice';
import { lightTheme } from '~/store/Slice/AppSlice';

const Theme = () => {
    const dispatch = useAppDispatch();
    const theme = useAppSelector((state) => state.app.theme);

    const toggleTheme = () => {
        if (theme === 'light') {
            dispatch(darkTheme());
        } else {
            dispatch(lightTheme());
        }
    };

    React.useEffect(() => {
        document.body.setAttribute('data-theme', theme);
    }, [theme]);

    return (
        <div className={styles.layoutTheme} onClick={toggleTheme}>
            {theme === 'light' ? (
                <IoMoonOutline className={styles.layoutThemeIcon} />
            ) : (
                <IoMoon className={styles.layoutThemeIcon} />
            )}{' '}
            <p className={styles.layoutThemeText}>{theme} Mode</p>
        </div>
    );
};

export default Theme;
