import React from 'react';
import useAppSelector from '~/hooks/useAppSelector';
import { urlsMap } from '~/routes/urls';
import { useNavigate } from 'react-router-dom';

interface IBorderCountries {
    borders?: string[] | undefined;
    className?: string;
}

const BorderCountries = ({ borders, className }: IBorderCountries) => {
    const countries = useAppSelector((state) => state.country.list);
    const borderCountries = countries.filter((item) => borders?.includes(item.cioc));
    const navigate = useNavigate();

    const handleClick = React.useCallback(
        (name: string) => {
            navigate(urlsMap.country + name);
        },
        [navigate],
    );

    return (
        <div className={className}>
            <div>
                <strong>Border Countries:</strong>
            </div>
            {!!borderCountries?.length
                ? borderCountries.map((item) => (
                      <p onClick={() => handleClick(item.name.common)}>{item.name.common}</p>
                  ))
                : 'There is no border country'}
        </div>
    );
};

export default BorderCountries;
