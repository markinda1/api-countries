import React from 'react';
import styles from './Card.module.scss';
import classNames from 'classnames';
import { useNavigate } from 'react-router-dom';
import { urlsMap } from '~/routes/urls';

interface ICard {
    country: ICountry;
    className?: string;
}

const Card = ({ country, className }: ICard) => {
    const navigate = useNavigate();

    const handleClick = React.useCallback(
        (name: string) => {
            navigate(urlsMap.country + name);
        },
        [navigate],
    );

    return (
        <div
            className={classNames(styles.card, className)}
            onClick={() => handleClick(country.name.common)}
        >
            <div className={styles.cardImage}>
                <img src={country.flags.png} alt={country.flags.alt} />
            </div>
            <div className={styles.cardContent}>
                <div className={styles.cardContentName}>
                    <strong>{country.name.common}</strong>
                </div>
                <p className={styles.cardContentItem}>
                    <strong>Population: </strong>
                    {country.population}
                </p>
                <p className={styles.cardContentItem}>
                    <strong>Region: </strong>
                    {country.region}
                </p>
                <p className={styles.cardContentItem}>
                    <strong>Capital: </strong>
                    {country.capital}
                </p>
            </div>
        </div>
    );
};

export default Card;
