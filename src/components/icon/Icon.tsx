import { INPUT_TYPE } from '~/const';
import { IoSearchSharp } from 'react-icons/io5';
import React from 'react';

interface IIcon {
    className?: string;
    type?: string | undefined;
}

const Icon = ({ type, className }: IIcon) => {
    switch (type) {
        case INPUT_TYPE.SEARCH:
            return <IoSearchSharp className={className} />;
        default:
            return null;
    }
};

export default Icon;
