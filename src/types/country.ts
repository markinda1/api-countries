declare global {
    interface ICountry {
        flags: {
            png: string;
            svg: string;
            alt: string;
        };
        name: {
            common: string;
            official: string;
            nativeName: INativeName;
        };
        borders: string[];
        cioc: string;
        currencies: ICurrency[];
        nativeName: string;
        capital: string[];
        region: string;
        population: number;
        tld: string[];
        languages: string[];
        subregion: string;
    }

    interface ICurrency {
        code: string;
        name: string;
        symbol: string;
    }

    interface INativeName {
        common: string;
    }
}

export default global;
