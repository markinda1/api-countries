import rootReducer, { setupStore } from '~/store/rootReducer';

declare global {
    type RootState = ReturnType<typeof rootReducer>;
    type AppStore = ReturnType<typeof setupStore>;
    type AppDispatch = AppStore['dispatch'];
}

export default global;
