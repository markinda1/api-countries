import { SingleValue } from 'react-select';

declare global {
    interface ISelectOption {
        [value: string]: string;
    }

    type ISelectValue = SingleValue<{ value: string; label: string }>;
}

export default global;
