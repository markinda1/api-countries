import * as React from 'react';
import { unstable_HistoryRouter as HistoryRouter, Routes, Route } from 'react-router-dom';

import { appHistory } from '~/routes/history';
import { urlsMap } from '~/routes/urls';
import Main from '~/containers/Main/Main';
import Country from '~/containers/Country/Country';

const Routing = () => (
    <HistoryRouter history={appHistory}>
        <Routes>
            <Route path={urlsMap.index}>
                <Route index element={<Main />} />
                <Route path={urlsMap.country + ':name'} element={<Country />} />
            </Route>
        </Routes>
    </HistoryRouter>
);

export default Routing;
